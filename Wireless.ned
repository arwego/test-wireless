package test_wireless;

import inet.common.figures.DelegateSignalConfigurator;
import inet.networklayer.configurator.ipv4.IPv4NetworkConfigurator;
import inet.node.inet.INetworkNode;
import inet.physicallayer.contract.packetlevel.IRadioMedium;
import inet.visualizer.integrated.IntegratedCanvasVisualizer;
import inet.environment.common.PhysicalEnvironment;

network Wireless
{
    parameters:
        string hostType = default("WirelessHost");
        string mediumType = default("IdealRadioMedium");

        @display("bgb=1000,500;bgg=100,1,grey95");
        @figure[title](type=label; pos=0,-1; anchor=sw; color=darkblue);

		//received packet text counter
        @figure[rcvdPkText](type=indicatorText; pos=700,20; anchor=w; font=,20; textFormat="Packets Received: %g"; initialValue=0);
        @statistic[rcvdPk](source=destination_rcvdPk; record=figure(count); targetFigure=rcvdPkText);
        @signal[destination_rcvdPk];
        @delegatesignal[rcvdPk](source=destination.udpApp[0].rcvdPk; target=destination_rcvdPk);

		//sent packet text counter
        @figure[sentPkTextA](type=indicatorText; pos=700,50; anchor=w; font=,20; textFormat="A Packets Sent: %g"; initialValue=0);
        @statistic[sentPkA](source=hostA_sentPk; record=figure(count); targetFigure=sentPkTextA);
        @signal[hostA_sentPk];
        @delegatesignal[sentPkA](source=hostA.udpApp[0].sentPk; target=hostA_sentPk);

        @figure[sentPkTextB](type=indicatorText; pos=700,80; anchor=w; font=,20; textFormat="B Packets Sent: %g"; initialValue=0);
        @statistic[sentPkB](source=hostB_sentPk; record=figure(count); targetFigure=sentPkTextB);
        @signal[hostB_sentPk];
        @delegatesignal[sentPkB](source=hostB.udpApp[0].sentPk; target=hostB_sentPk);

        @figure[sentPkTextC](type=indicatorText; pos=700, 110; anchor=w; font=,20; textFormat="C Packets Sent: %g"; initialValue=0);
        @statistic[sentPkC](source=hostC_sentPk; record=figure(count); targetFigure=sentPkTextC);
        @signal[hostC_sentPk];
        @delegatesignal[sentPkC](source=hostC.udpApp[0].sentPk; target=hostC_sentPk);

        @figure[sentPkTextD](type=indicatorText; pos=700,140; anchor=w; font=,20; textFormat="D Packets Sent: %g"; initialValue=0);
        @statistic[sentPkD](source=hostD_sentPk; record=figure(count); targetFigure=sentPkTextD);
        @signal[hostD_sentPk];
        @delegatesignal[sentPkD](source=hostD.udpApp[0].sentPk; target=hostD_sentPk);

        @figure[sentPkTextE](type=indicatorText; pos=700,170; anchor=w; font=,20; textFormat="E Packets Sent: %g"; initialValue=0);
        @statistic[sentPkE](source=hostE_sentPk; record=figure(count); targetFigure=sentPkTextE);
        @signal[hostE_sentPk];
        @delegatesignal[sentPkE](source=hostE.udpApp[0].sentPk; target=hostE_sentPk);

        @figure[sentPkTextF](type=indicatorText; pos=700,200; anchor=w; font=,20; textFormat="F Packets Sent: %g"; initialValue=0);
        @statistic[sentPkF](source=hostF_sentPk; record=figure(count); targetFigure=sentPkTextF);
        @signal[hostF_sentPk];
        @delegatesignal[sentPkF](source=hostF.udpApp[0].sentPk; target=hostF_sentPk);

        @figure[sentPkTextG](type=indicatorText; pos=700,230; anchor=w; font=,20; textFormat="G Packets Sent: %g"; initialValue=0);
        @statistic[sentPkG](source=hostG_sentPk; record=figure(count); targetFigure=sentPkTextG);
        @signal[hostG_sentPk];
        @delegatesignal[sentPkG](source=hostG.udpApp[0].sentPk; target=hostG_sentPk);

    submodules:
        physicalEnvironment: PhysicalEnvironment {
            @display("p=950,450");
        }
        visualizer: IntegratedCanvasVisualizer {
            @display("p=950,50");
        }
        configurator: IPv4NetworkConfigurator {
            @display("p=950,150");
        }
        radioMedium: <mediumType> like IRadioMedium {
            @display("p=950,250");
        }
        figureHelper: DelegateSignalConfigurator {
            @display("p=950,350");
        }

        //sender
        hostA: <hostType> like INetworkNode {
            @display("p=135,100");
        }
        hostB: <hostType> like INetworkNode {
            @display("p=85,240");
        }
        hostC: <hostType> like INetworkNode {
            @display("p=325,100");
        }
        hostD: <hostType> like INetworkNode {
            @display("p=165,380");
        }
        hostE: <hostType> like INetworkNode {
            @display("p=420,240");
        }
        hostF: <hostType> like INetworkNode {
            @display("p=325,380");
        }
        hostG: <hostType> like INetworkNode {
            @display("p=240,240");
        }

        //receiver / sink-node
        destination: <hostType> like INetworkNode {
            @display("p=585,240");
        }

}
